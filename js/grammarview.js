function formatGrammar(grammar) {
	var result = "<div class=\"col-lg-1\" style=\"margin-right:0; padding:0\"><textarea  class=\"form-control\" id=\"ruleIndices\" rows=\"25\" cols=\"5\" disabled></textarea></div>";
	result += "<div class=\"col-lg-11\ style=\"margin-left:0;padding:0\"><textarea class=\"form-control\"  id=\"grammar\" rows=\"25\" cols=\"35\" onfocus=\"$('ruleIndices').value = ''\" onblur=\"displayRuleIndices();\" onchange=\"grammarChanged();\">";
	for (var i in grammar.rules) {
		result += grammar.rules[i] + "\n";
	}
	result += "</textarea></div>";

	return result;
}

function displayRuleIndices() {
	var rules = $('grammar').value.split('\n');
	var ruleIndex = 0;

	$('ruleIndices').value = "";

	for (var i in rules) {
		if (rules[i].trim() != '') {
			$('ruleIndices').value += (ruleIndex++) ;
		}

		$('ruleIndices').value += "\n";
	}
}

function formatFirstFollow(grammar) {
	var result = "<table class=\"table table-responsive table-striped table-hover\">";

	result += "<thead><tr><th>FIRST table</th></tr><tr><th>Nonterminal</th><th>FIRST</th></thead><tbody>"
	for (var i in grammar.nonterminals) {
		var nonterminal = grammar.nonterminals[i];
		result += "<tr><td>" + nonterminal + "</td><td>{" + grammar.firsts[nonterminal] + "}</td></tr>";
	}
	result += "</tbody>"
	result += "</table>";

	return result;
}
